package com.csnoozy.app.views

import com.csnoozy.app.controllers.jda.JDAController
import net.dv8tion.jda.core.JDA
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/28/2018
*
* File: com.csnoozy.app.views.JDALoadingView
* Description:
*/

class JDALoadingView : View("My View") {

    private val jdaController : JDAController by inject()

    override val root = borderpane {
        top {
            text("Logging In!")
        }
        bottom {
            text {
                jdaController.jdaStatus.subscribe { text = it }
            }
        }
    }

    override fun onDock() {
        currentWindow?.sizeToScene()
        jdaController.jdaStatus.subscribe {
            if (it == JDA.Status.CONNECTED.name) {
                runLater {
                    replaceWith(DiscordPrimaryView::class)
                }
            }
        }
    }
}
