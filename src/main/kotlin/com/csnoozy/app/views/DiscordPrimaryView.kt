package com.csnoozy.app.views

import javafx.scene.Parent
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/28/2018
*
* File: com.csnoozy.app.views.DiscordPrimaryView
* Description:
*/

/**
 *
 */
class DiscordPrimaryView: View() {

    override val root = borderpane {
        top {
            text("This is text!")
        }
    }
}