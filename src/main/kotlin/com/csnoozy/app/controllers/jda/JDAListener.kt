package com.csnoozy.app.controllers.jda

import net.dv8tion.jda.core.events.ReadyEvent
import net.dv8tion.jda.core.hooks.ListenerAdapter

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/28/2018
*
* File: com.csnoozy.app.controllers.jda.JDAListener
* Description:
*/

class JDAListener(private val jdaController : JDAController): ListenerAdapter() {

    override fun onReady(event : ReadyEvent?) {
        println("JDA Ready")
    }
}