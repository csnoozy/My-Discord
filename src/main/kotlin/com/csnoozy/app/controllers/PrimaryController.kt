package com.csnoozy.app.controllers

import com.github.thomasnield.rxkotlinfx.bind
import com.github.thomasnield.rxkotlinfx.toObservable
import io.reactivex.subjects.BehaviorSubject
import io.reactivex.subjects.PublishSubject
import javafx.beans.property.SimpleStringProperty
import javafx.event.ActionEvent
import tornadofx.*

/*
* Project: my-discord
* 
* Author: Caleb Snoozy
* Date: 4/23/2018
*
* File: com.csnoozy.app.controllers.PrimaryController
* Description:
*/

class PrimaryController : Controller() {

    private val tokenProperty = SimpleStringProperty("")
    var token by tokenProperty

    val applicationRunning = PublishSubject.create<Boolean>()

    val tokenField = PublishSubject.create<String>().apply {
        tokenProperty.bind(this)
    }

    val submitToken = BehaviorSubject.create<ActionEvent>().apply {
        subscribe {
            println("Token Submitted $token")
            true.toProperty().toObservable().subscribe(jdaLoad)
        }
    }

    val jdaLoad = PublishSubject.create<Boolean>()
}
